package com.teparak.graderbot.assignment.master

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan("com.teparak.graderbot")
class AssignmentMasterApplication

fun main(args: Array<String>) {
    runApplication<AssignmentMasterApplication>(*args)
}
