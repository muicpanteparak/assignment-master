package com.teparak.graderbot.assignment.master.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("container")
class ContainerDriverConfig {

    var driver: String? = null
    var apiKey: String? = null
}