package com.teparak.graderbot.assignment.master.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor


@Configuration
class ExecutorConfiguration {

    @Bean
    fun eventWatcherExecutor(): ThreadPoolTaskExecutor {
        val pool = ThreadPoolTaskExecutor()
        pool.corePoolSize = 1
        pool.maxPoolSize = 1
        pool.setThreadNamePrefix("event_watcher_executor")
        return pool
    }

    @Bean
    fun logListenerExecutor(): ThreadPoolTaskExecutor {
        val pool = ThreadPoolTaskExecutor()
        pool.corePoolSize = Runtime.getRuntime().availableProcessors()
        pool.setThreadNamePrefix("log_listener_executor")
        return pool
    }
}