package com.teparak.graderbot.assignment.master.launcher

import com.teparak.graderbot.assignment.master.minioncontroller.MinionDriver
import com.teparak.graderbot.storage.StorageService
import com.teparak.graderbot.utils.RandomFactory
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.nio.file.Paths
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream


@Service
class ContainerManagement {

    companion object {
        private val logger = LoggerFactory.getLogger(ContainerManagement::class.java)
    }

    @Autowired
    @Qualifier("docker")
    private lateinit var minionDriver: MinionDriver

    @Autowired
    private lateinit var minionImageConfiguration: MinionImageConfiguration

    @Autowired
    private lateinit var randomFactory: RandomFactory

    @Autowired
    private lateinit var containerMetadataService: ContainerMetadataService

    @Autowired
    private lateinit var storageService: StorageService

    @RabbitListener(queues = [LaunchQueueConfiguration.TASK_QUEUE])
    private fun processQueue(newTask: SubmitAssignmentMetadata) {
        launch(newTask)
    }

    fun launch(newTask: SubmitAssignmentMetadata) {
        logger.debug(newTask.toString())

        val (language, tests, files) = newTask

        val (image, version, command) = minionImageConfiguration.configuration(language)

        logger.debug("Launching new task with '$image:$version'")
        logger.debug("Launch command: ${command.joinToString(separator = " ")}")

        val file = creatingZip(tests)

        val ok = randomFactory.alphanumericalIgnoreCase()

        storageService.putObject("temp", ok, file)

        storageService.createBucket("temp")
        val testUrl = storageService.presignedDownloadURL(bucket = "temp", objectName = ok, attachmentName = "tests.zip")

        val submUrl = storageService.presignedDownloadURL(bucket = files.bucket, objectName = files.objectKey, attachmentName = files.originalName)

        val env = mutableMapOf(
                Pair("SUBMIT_URL", submUrl.toString()),
                Pair("SUBMIT_FILENAME", files.originalName),
                Pair("TEST_URL", testUrl.toString()),
                Pair("TEST_FILENAME", "tests.zip")
        )

        logger.debug(env.toList().joinToString { pair: Pair<String, String> -> pair.toString() })

        val containerCallback : (String) -> Unit = {
            val meta = CreateMetadata(it, image = image, version = version, submissionUid = newTask.submissionUid, submBucket = files.bucket, submKey = files.objectKey)
            containerMetadataService.createMetadata(meta)
        }

        minionDriver.launch(
                image =image,
                imagePullPolicy = "always",
                env = env,
                callback = containerCallback
        )
    }

    fun creatingZip(tests: List<TestCaseMetadata>): File {

        val buffer = ByteArray(8192)
        val rand = randomFactory.alphanumerical(10)
        logger.debug("Creating zip '$rand'")

        val folder = Paths.get("/", "tmp", rand).toFile()
        folder.mkdirs()

        val zipPath = Paths.get(folder.absolutePath, "tests.zip").toFile()

        logger.debug("Path At: '${zipPath.absolutePath}")

        val zip = ZipOutputStream(FileOutputStream(zipPath))

        tests.forEach {
            val (inputStream, meta ) = storageService.getObject(it.bucket, it.objectKey)
//            val file = Paths.get(rand, it.originalName).toFile()
//            FileUtils.copyToFile(inputStream, file)
            try {
                val entry = ZipEntry(it.originalName).apply {
                    this.size = meta.contentLength
                }

                logger.debug("Adding '${it.originalName}' to zip")
                zip.putNextEntry(entry)

                var length: Int = inputStream.read(buffer)

                while (length > 0) {
                    zip.write(buffer, 0, length)

                    length = inputStream.read(buffer)
                }

                zip.closeEntry()
            } catch (e: IOException){
                e.printStackTrace()

            }finally {
                zip.closeEntry()
                inputStream.close()
            }
        }

        zip.close()
        return zipPath
    }
}

sealed class RabbitMessage
sealed class ObjectMeta(open val bucket: String, open val objectKey: String, open val originalName: String)
data class TestCaseMetadata(override val bucket: String, override val objectKey: String, override val originalName: String) : ObjectMeta(bucket, objectKey, originalName)
data class SubmissionMetadata(override val bucket: String, override val objectKey: String, override val originalName: String) : ObjectMeta(bucket, objectKey, originalName)
data class SubmitAssignmentMetadata(val language: String, val tests: List<TestCaseMetadata>, val files: SubmissionMetadata, val submissionUid: String) : RabbitMessage()