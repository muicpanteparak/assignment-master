package com.teparak.graderbot.assignment.master.launcher

import com.teparak.graderbot.utils.entity.AbstractEntity
import javax.persistence.*


@Entity
@Table(indexes = [
    Index(columnList = "containerId", unique = true)
])
class ContainerMetadata(

        @Column(unique = true)
        var containerId: String? = null,

        @Column
        var image: String? = null,

        @Column
        var imageVersion: String? = null,


//        @OrderColumn
//        @ElementCollection(targetClass = String::class)
//        @CollectionTable
//        var command: List<String> = listOf(),

        var exitCode: Int? = null,


        @Column(nullable = false)
        @Enumerated(EnumType.STRING)
        var status: ContainerStatus = ContainerStatus.JOB_RECEIVED,


        @Column(nullable = false, unique = true)
        var submissionUid: String,

        @Column(nullable = false)
        var submBucket: String,

        @Column(nullable = false)
        var submKey: String

) : AbstractEntity()