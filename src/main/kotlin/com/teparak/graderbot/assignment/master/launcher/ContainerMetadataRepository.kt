package com.teparak.graderbot.assignment.master.launcher

import org.springframework.data.jpa.repository.JpaRepository

interface ContainerMetadataRepository : JpaRepository<ContainerMetadata, Long> {
    fun findByContainerId(containerId: String): ContainerMetadata?
    fun existsByContainerId(containerId: String): Boolean
}