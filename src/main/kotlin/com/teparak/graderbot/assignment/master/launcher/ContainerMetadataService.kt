package com.teparak.graderbot.assignment.master.launcher

import com.teparak.graderbot.assignment.master.minioncontroller.Message
import com.teparak.graderbot.assignment.master.minioncontroller.MessageType
import com.teparak.graderbot.assignment.master.minioncontroller.ResultNotifier
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ContainerMetadataService {

    @Autowired
    private lateinit var containerMetadataRepository: ContainerMetadataRepository

    @Autowired
    private lateinit var notifier: ResultNotifier

    @Transactional
    fun createMetadata(createMetadata: CreateMetadata){
        if (!containerExist(createMetadata.containerId)) {
            val metadata = ContainerMetadata(
                    containerId = createMetadata.containerId,
                    imageVersion = createMetadata.version,
                    image = createMetadata.image,
                    submissionUid = createMetadata.submissionUid,
                    submBucket = createMetadata.submBucket,
                    submKey = createMetadata.submKey,
                    status = ContainerStatus.JOB_RECEIVED
            )
            containerMetadataRepository.save(metadata)
        } else {
            findByContainerId(createMetadata.containerId).apply {
                this.imageVersion = createMetadata.version
                this.image = createMetadata.image
                this.submissionUid = createMetadata.submissionUid
                this.submBucket = createMetadata.submBucket
                this.submKey = createMetadata.submKey
                this.status = ContainerStatus.JOB_RECEIVED
            }
        }

        notifier.sentNotification(Message(message = ContainerStatus.JOB_RECEIVED.toString(), submissionUid = createMetadata.submissionUid, type = MessageType.EVENT))
    }

    @Transactional
    fun containerCreated(containerStarted: ContainerStarted): ContainerMetadata {

        return findByContainerId(
                containerId = containerStarted.containerid
        ).apply {
            this.status = ContainerStatus.CREATED

            notifier.sentNotification(Message(submissionUid = this.submissionUid, message = ContainerStatus.CREATED.toString(), type = MessageType.EVENT))
        }
    }

    @Transactional
    fun containerStarted(containerStarted: ContainerStarted): ContainerMetadata {
        return findByContainerId(
                containerId = containerStarted.containerid
        ).apply {
            this.status = ContainerStatus.STARTED
            notifier.sentNotification(Message(submissionUid = this.submissionUid, message = ContainerStatus.STARTED.toString(),  type = MessageType.EVENT))
        }
    }

    @Transactional
    fun containerDied(containerDied: ContainerDied): ContainerMetadata{
        return findByContainerId(
                containerId = containerDied.containerid
        ).apply {
            this.status = ContainerStatus.DIED
            this.exitCode = containerDied.exitCode

            notifier.sentNotification(Message(submissionUid = this.submissionUid, order = this.exitCode ?: -1 ,message = ContainerStatus.DIED.toString(),  type = MessageType.EVENT))
        }
    }

    @Transactional
    fun findByContainerId(containerId: String): ContainerMetadata{
        return containerMetadataRepository.findByContainerId(
                containerId = containerId
        ) ?: throw Exception("ContainerMetadata Not found. (Concurrency)")
    }

    @Transactional
    fun containerKilled(container: ContainerKilled): ContainerMetadata {
        return findByContainerId(containerId = container.containerId)
                .apply {
                    this.status = ContainerStatus.KILLED
                    notifier.sentNotification(Message(submissionUid = this.submissionUid, message = ContainerStatus.KILLED.toString(),  type = MessageType.EVENT))
                }
    }

    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
    fun isWatchingContainer(containerId: String): Boolean {
        return containerExist(containerId)
    }

    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
    private fun containerExist(containerId: String): Boolean {
        return containerMetadataRepository.existsByContainerId(
                containerId = containerId
        )
    }
}

data class ContainerStarted(val containerid: String)
data class ContainerDied(val containerid: String, val exitCode: Int)
data class ContainerKilled(val containerId: String)
data class CreateMetadata(
        val containerId: String,
        val image: String,
        val version: String,
        val submissionUid: String,
        val submBucket: String,
        val submKey: String)