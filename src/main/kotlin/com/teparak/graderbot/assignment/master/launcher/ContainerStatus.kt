package com.teparak.graderbot.assignment.master.launcher

enum class ContainerStatus {
    JOB_RECEIVED, STARTED, DIED, KILLED, CREATED
}