package com.teparak.graderbot.assignment.master.launcher

import org.springframework.amqp.core.*
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class LaunchQueueConfiguration {

    companion object {
        const val TASK_QUEUE = "task.queue.executor"
        const val TASK_EXCHANGE = "task.exchange"
    }

    @Bean
    fun taskExchange(): Exchange {
        return ExchangeBuilder.topicExchange(TASK_EXCHANGE).durable(true).build()
    }

    @Bean
    fun binding(queue: Queue, taskExchange: Exchange): Binding {
        return BindingBuilder
                .bind(queue)
                .to(taskExchange)
                .with(queue.name)
                .noargs()
    }

    @Bean
    fun queue(): Queue {
        return QueueBuilder
                .durable(TASK_QUEUE)
                .build()
    }
}