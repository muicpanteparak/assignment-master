package com.teparak.graderbot.assignment.master.launcher

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

//@PropertySource("classpath:image.yml", ignoreResourceNotFound=false, encoding = "utf-8")
//@Configuration
//@ConfigurationProperties("image")
@Component
class MinionImageConfiguration {

    companion object {
        private val logger = LoggerFactory.getLogger(MinionImageConfiguration::class.java)
    }

    private val image: MutableMap<String, ImageMetadata>  = hashMapOf(
            Pair("python3", ImageMetadata(image = "panteparak/python-sandbox", command = mutableListOf("pytest", "-p", "no:cacheprovider"))),
            Pair("python2", ImageMetadata(image = "panteparak/python-sandbox", command = mutableListOf("pytest", "-p", "no:cacheprovider"))),
            Pair("python", ImageMetadata(image = "panteparak/python-sandbox", command = mutableListOf("pytest", "-p", "no:cacheprovider")))
    )

    fun configuration(language: String): ImageMetadata {
        logger.debug("Getting configuration for '$language'")
        return image[language]!!
    }
}

data class ImageMetadata(val image: String, val version: String="latest", val command: MutableList<String>)