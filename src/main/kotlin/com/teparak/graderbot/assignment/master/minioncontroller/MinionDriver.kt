package com.teparak.graderbot.assignment.master.minioncontroller

interface MinionDriver {
    fun launch(containerName: String? = null, image: String, command: List<String>? = null, imagePullPolicy: String, env: Map<String, String>, callback: ((String) -> Unit)?): String
    fun list()
    fun stop(containerId: String, secondsBeforeKilling: Int=10)
    fun kill(containerId: String)
    fun describe(containerId: String)
    fun createVolume(volumeName: String)
    fun listVolume()
    fun removeVolume(volumeName: String)
    fun watch(): Any
    fun logs(containerId: String): Any
}