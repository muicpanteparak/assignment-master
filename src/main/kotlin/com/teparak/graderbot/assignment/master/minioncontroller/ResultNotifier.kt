package com.teparak.graderbot.assignment.master.minioncontroller

import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Service

@Service
class ResultNotifier(
        private val notify: RabbitTemplate
) {

    companion object {
        private val logger = LoggerFactory.getLogger(ResultNotifier::class.java)
    }

    fun sentMessage(message: Message) {
        message.apply {
            this.type = MessageType.MESSAGE
        }

        logger.debug("Sending MESSAGE: SubmissionUID ${message.submissionUid} Order ${message.order}")
        notify.convertAndSend(ResultQueueConfiguration.EXCHANGE, ResultQueueConfiguration.QUEUE, message)
    }

    fun sentNotification(event: Message) {
        event.apply {
            this.type = MessageType.EVENT
        }
        notify.convertAndSend(ResultQueueConfiguration.EXCHANGE, ResultQueueConfiguration.QUEUE, event)
        logger.debug("Sending EVENT: SubmissionUID ${event.submissionUid}")
    }
}

enum class MessageType {
    MESSAGE, EVENT
}


sealed class BaseMessage(open var type: MessageType = MessageType.MESSAGE, open val submissionUid: String)
data class Message(val message: String, val order: Int = 0, override val submissionUid: String, override var type: MessageType) : BaseMessage(type, submissionUid)