package com.teparak.graderbot.assignment.master.minioncontroller.docker

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("launcher.docker")
class DockerConfiguration {

    lateinit var networkId: String
}