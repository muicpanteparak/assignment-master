package com.teparak.graderbot.assignment.master.minioncontroller.docker

import com.spotify.docker.client.messages.Event
import com.teparak.graderbot.assignment.master.launcher.ContainerDied
import com.teparak.graderbot.assignment.master.launcher.ContainerKilled
import com.teparak.graderbot.assignment.master.launcher.ContainerMetadataService
import com.teparak.graderbot.assignment.master.launcher.ContainerStarted
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Scope
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.stereotype.Component

@Scope("prototype")
@Component
class DockerEventDaemon(
     val minionDriver: DockerMinionDriver,
     val containerMetadataService: ContainerMetadataService,

     @Qualifier("logListenerExecutor")
     val taskExecutor: ThreadPoolTaskExecutor,
     val appContext: ApplicationContext
) : Runnable {

    companion object {
        private val logger = LoggerFactory.getLogger(DockerEventDaemon::class.java)
        private val watchAction = arrayOf("start", "kill", "die", "create")
    }

    override fun run() {
        val eventStream = minionDriver.watch()

        var event: Event? = null
        while ({ event = eventStream.next() ?: null; event }() != null) {


            val action = event!!.action()!!
            val containerId = event!!.actor().id()!!


            if (!isValidAction(action)){
                if (logger.isDebugEnabled){
                    logger.debug("INVALID ACTION - ignoring container '$containerId' action: '$action'")
                }
                continue
            }

            if (!containerMetadataService.isWatchingContainer(containerId)){
                if (logger.isDebugEnabled){
                    logger.debug("Non observing Container - Ignoring container '$containerId' action: '$action'")
                }
                continue
            }

            when {
                isDie(action) -> {
                    val exitCode = event!!.actor().attributes()["exitCode"]!!
                    logger.debug("Detect Event: Container '$containerId' action: '$action' exitcode: '$exitCode'")

                    containerMetadataService.containerDied(ContainerDied(
                            containerid = containerId,
                            exitCode = exitCode.toInt()

                    ))
                }

                isStart(action) -> {
                    logger.debug("Detect Event: Container: '$containerId' action: '$action'")
                    val container = containerMetadataService.containerStarted(ContainerStarted(
                            containerid = containerId
                    ))

                    val instance: DockerLogDaemon = appContext.getBean(DockerLogDaemon::class.java).apply {
                        this.containerId = containerId
                        this.submissionUid = container.submissionUid
                        this.bucket = container.submBucket
                        this.key = container.submKey
                    }
                    taskExecutor.execute(instance)
                }

                isKill(action) -> {
                    logger.warn("Detect Event: Container '$containerId' action: '$action'")

                    containerMetadataService.containerKilled(ContainerKilled(
                            containerId = containerId
                    ))
                }

                isCreated(action) -> {
                    logger.debug("Detect Event: Container: '$containerId' action: '$action'")

                }
            }
        }
    }

    private fun isKill(action: String) = action.equals("kill", true)
    private fun isStart(action: String) = action.equals("start", true)
    private fun isDie(action: String) = action.equals("die", true)
    private fun isCreated(action: String) = action.equals("create", true)
    private fun isValidAction(action: String): Boolean = watchAction.contains(action)
}