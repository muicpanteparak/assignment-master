package com.teparak.graderbot.assignment.master.minioncontroller.docker

import com.spotify.docker.client.LogStream
import com.teparak.graderbot.assignment.master.minioncontroller.Message
import com.teparak.graderbot.assignment.master.minioncontroller.MessageType
import com.teparak.graderbot.assignment.master.minioncontroller.ResultNotifier
import com.teparak.graderbot.storage.StorageService
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.io.BufferedWriter
import java.io.FileWriter
import java.io.IOException
import java.io.PrintWriter
import java.nio.ByteBuffer
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Paths


@Component
@Scope("prototype")
class DockerLogDaemon(
        private val minionDriver: DockerMinionDriver,
        private val outputNotify: ResultNotifier,
        private val storageService: StorageService
) : Runnable {

    companion object {
        private val logger = LoggerFactory.getLogger(DockerLogDaemon::class.java)
    }

    var containerId: String? = null
    var submissionUid: String? = null
    var bucket: String? = null
    var key: String? = null


    override fun run() {

        if (containerId.isNullOrBlank()) {
            logger.error("containerId cannot be blank or null")
            return
        }

        if (submissionUid.isNullOrBlank()){
            logger.error("Submission UID cannot be blank or null")
            return
        }

        if (bucket.isNullOrBlank() || key.isNullOrBlank()){
            logger.error("Bucket or Key cannot be null or blank '$bucket:$key'")
            return
        }

        val out: PrintWriter?
        val file = Paths.get("/", "tmp", "$submissionUid.out").toFile()

        try {
            out = PrintWriter(BufferedWriter(FileWriter(file, true)))
        } catch (e: IOException){
            logger.error(e.message)
            throw e
        }

        var count = 0

        val logStream = minionDriver.logs(containerId!!) as LogStream

        logger.debug("Log Stream Starting for container '$containerId'")
        while (logStream.hasNext() && count <= 2500){
            val buffer = logStream.next().content()!!

            val (st, end, sz) = extractMeta(buffer = buffer)

            val bytes = ByteArray(sz)
            buffer.get(bytes, st, end)

            val line = String(bytes, Charset.defaultCharset())

            if (shouldNotify(line)){
                outputNotify.sentMessage(
                        Message(
                                message = line,
                                submissionUid = submissionUid!!,
                                order = count++,
                                type = MessageType.MESSAGE
                        )
                )
            }

            // Write to file
            out.print(line)
        }

        out.close()

        storageService.putObject(bucket = bucket!!, objectName = "log/$submissionUid.out", file = file)
        Files.deleteIfExists(file.toPath())
        // Upload to storage
        logger.debug("Log Stream Ended for container '$containerId'")
    }

    private fun shouldSplit(line: String): Boolean = line.length > 524288 // ~ 50MB

    // Notify Rules
    private fun shouldNotify(line: String): Boolean  = line.length in 0..128000

    private fun extractMeta(buffer: ByteBuffer): Triple<Int, Int, Int> = Triple(buffer.position(), buffer.limit(), buffer.capacity())
}