package com.teparak.graderbot.assignment.master.minioncontroller.docker

import com.spotify.docker.client.DefaultDockerClient
import com.spotify.docker.client.DockerClient
import com.spotify.docker.client.EventStream
import com.spotify.docker.client.exceptions.ConflictException
import com.spotify.docker.client.exceptions.VolumeNotFoundException
import com.spotify.docker.client.messages.ContainerConfig
import com.spotify.docker.client.messages.Event
import com.spotify.docker.client.messages.HostConfig
import com.spotify.docker.client.messages.Volume
import com.teparak.graderbot.assignment.master.minioncontroller.MinionDriver
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import javax.annotation.PostConstruct


@Configuration
@Qualifier("docker")
class DockerMinionDriver(
        val dockerConfiguration: DockerConfiguration,
        val applicationContext: ApplicationContext,

        @Qualifier("eventWatcherExecutor")
        val taskExecutor: ThreadPoolTaskExecutor
) : MinionDriver {

    companion object {
        private const val unixSocket = "unix:///var/run/docker.sock"
        private val logger = LoggerFactory.getLogger(DockerMinionDriver::class.java)
    }

    private lateinit var docker: DefaultDockerClient

    init {
        logger.debug("Connecting to Docker socket at '$unixSocket'")
        docker = DefaultDockerClient
                .fromEnv()
                .useProxy(false)
                .connectionPoolSize(100)
                .uri(unixSocket)
                .build()

    }

    @PostConstruct
    fun init(){
        logger.debug("Creating Watch Event")
        taskExecutor.execute(applicationContext.getBean(DockerEventDaemon::class.java))
    }

    override fun launch(containerName: String?, image: String, command: List<String>?, imagePullPolicy: String, env: Map<String, String>, callback: ((String) -> Unit)?): String {
        this.pull(image)

        val transformEnv = env
                .toList()
                .map {
                    itm -> "${itm.first}=${itm.second}"
                }

        logger.debug("Creating launch configuration")

        val hostConfig = HostConfig
                .builder()
                .networkMode(if (dockerConfiguration.networkId.equals("host", true)) "host" else null)
                .build()

        val containerConfig = ContainerConfig
                .builder()
                .hostConfig(hostConfig)
                .image(image)
                .cmd(command)
                .env(transformEnv)
                .build()


        val creation = docker.createContainer(containerConfig, containerName)

        val containerId = creation.id().toString()
        logger.debug("Container Created '$containerId'")

        docker.connectToNetwork(containerId, dockerConfiguration.networkId)

        callback?.invoke(containerId)

        logger.debug("Launching docker container")
        docker.startContainer(containerId)

        logger.debug("Container launched with id: '$containerId' with command '$command'")
        return containerId
    }

    override fun kill(containerId: String) {
        killContainer(containerId)
    }

    override fun stop(containerId: String, secondsBeforeKilling: Int) {
        stopContainer(containerId, secondsBeforeKilling)
    }

    override fun describe(containerId: String) {
        docker.inspectContainer(containerId)
    }

    override fun list() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun pull(image: String, tag: String = "latest") {
        logger.debug("Pulling Image from '$image:$tag'")
        docker.pull("$image:$tag")
    }

    private fun stopContainer(containerId: String, secondsBeforeKilling: Int=10) {
        logger.debug("Stopping container id: '$containerId'")
        docker.stopContainer(containerId, secondsBeforeKilling)
    }

    private fun killContainer(containerId: String){
        logger.debug("Killing container '$containerId'")
        docker.killContainer(containerId)
        logger.debug("Container '$containerId' is killed")
    }

    override fun createVolume(volumeName: String) {

        val vol = Volume
                .builder()
                .name(volumeName)
                .build()

        logger.debug("volume configuration created for  '$volumeName'")

        docker.createVolume(vol)
        logger.debug("Volume created")
    }

    override fun listVolume() {
        docker.listVolumes()
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    @Throws(VolumeNotFoundException::class, ConflictException::class)
    override fun removeVolume(volumeName: String) {
        docker.removeVolume(volumeName)
        logger.debug("volume removed: '$volumeName'")
    }

    override fun watch(): EventStream {
        return docker.events(DockerClient.EventsParam.type(Event.Type.CONTAINER))
    }

    override fun logs(containerId: String): Any {

        return docker.logs(containerId,
                DockerClient.LogsParam.stdout(),
                DockerClient.LogsParam.stderr(),
                DockerClient.LogsParam.timestamps(false),
                DockerClient.LogsParam.follow()
        )!!
    }
}

//fun main() {
//    val docker = DockerMinionDriver()
//
//    var event: Event? = docker.watch().next()
//    while (event != null){
//
//        val action = event.action()!!
//        val containerId = event.actor().id()!!
//
//        if (action.equals("die", true)){
//            val exitCode = event.actor().attributes()["exitCode"]!!
//            println("ContainerID: $containerId Status: $action ExitCode: $exitCode")
//        } else if (action.equals("start", true)){
//            println("ContainerID: $containerId Status: $action")
//        }
//
//        event = docker.watch().next()
//    }


//    val docker = DockerMinionDriver()
//
//    val logStream = docker.logs("3d0b3ddf441d") as LogStream
//    println("BEGIN")
//    logStream.attach(System.out, System.err)
////    logStream
//    println("END")
//}