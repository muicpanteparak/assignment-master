package com.teparak.graderbot.assignment.master.minioncontroller.kubernetes

import com.teparak.graderbot.assignment.master.minioncontroller.MinionDriver
import io.kubernetes.client.ApiException
import io.kubernetes.client.apis.BatchV1Api
import io.kubernetes.client.apis.CoreV1Api
import io.kubernetes.client.models.*
import io.kubernetes.client.util.Config
import org.springframework.beans.factory.annotation.Qualifier
import java.util.*

@Qualifier("kube")
class KubernetesMinionDriver : MinionDriver {
    val namespace: String
    val batchV1Api: BatchV1Api
    val coreV1Api: CoreV1Api

    constructor(apikey: String, namespace: String = "sandbox") {
        this.namespace = namespace

        val config  = Config
                .defaultClient()
        config.setApiKey(apikey)

        coreV1Api = CoreV1Api(config)
        batchV1Api = BatchV1Api(config)
    }

    @Throws(ApiException::class)
    override fun launch(containerName: String?, image: String, command: List<String>?, imagePullPolicy: String, env: Map<String, String>, callback: ((String) -> Unit)?): String {

        fun createContainer(name: String?, image: String, command: List<String>?, imagePullPolicy: String): V1Container {
            val container = V1Container()
            container.command = command
            container.name = name
            container.image = image
            container.imagePullPolicy = imagePullPolicy

            return container
        }

        fun createObjectMeta(name: String?, namespace: String): V1ObjectMeta {
            val pod_meta = V1ObjectMeta()
            pod_meta.name = name
            pod_meta.namespace = namespace

            return pod_meta
        }

        fun createPodSpec(container: V1Container, restartPolicy: String = "Never"): V1PodSpec {
            val pod_spec = V1PodSpec()
            pod_spec.containers = Arrays.asList(container)
            pod_spec.restartPolicy = restartPolicy

            return pod_spec
        }

        fun createTemplateSpec(podSpec: V1PodSpec, podMeta: V1ObjectMeta): V1PodTemplateSpec {
            val pod_template_spec = V1PodTemplateSpec()
            pod_template_spec.spec = podSpec
            pod_template_spec.metadata = podMeta

            return pod_template_spec
        }

        fun createJobSpec(podTemplateSpec: V1PodTemplateSpec): V1JobSpec {
            val job_spec = V1JobSpec()
            job_spec.template = podTemplateSpec

            return job_spec
        }

        val container = createContainer(containerName, image, command, imagePullPolicy)
        val podMeta = createObjectMeta(containerName, namespace)
        val podSpec = createPodSpec(container)
        val podTemplateSpec = createTemplateSpec(podMeta = podMeta, podSpec = podSpec)
        val jobMeta = createObjectMeta(containerName, namespace)
        val jobSpec = createJobSpec(podTemplateSpec = podTemplateSpec)

        val body = V1Job()
        body.spec = jobSpec
        body.metadata = jobMeta


        val name = batchV1Api.createNamespacedJob(this.namespace, body, false, "", "").metadata.name

        return name
    }

    override fun list() {
//        coreV1Api.listPodForAllNamespaces()
    }

    override fun stop(containerId: String, secondsBeforeKilling: Int) {
        throw NotImplementedError("Function not implemented")
    }

    override fun kill(containerId: String) {
        throw NotImplementedError("Function not implemented")
    }

    override fun describe(containerId: String) {
        throw NotImplementedError("Function not implemented")
    }

    override fun createVolume(volumeName: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun listVolume() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun removeVolume(volumeName: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun watch() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun logs(containerId: String): Any {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}