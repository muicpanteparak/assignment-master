package com.teparak.graderbot.storage

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotBlank

@Configuration
@ConfigurationProperties("storage")
@EnableConfigurationProperties
@Validated
class StorageServiceProperties {

    @NotBlank
    lateinit var  accessKey: String

    @NotBlank
    lateinit var secretKey: String

    @NotBlank
    var hostname: String = "localhost"
    var port: Int = 9000
    var secure: Boolean = false

}