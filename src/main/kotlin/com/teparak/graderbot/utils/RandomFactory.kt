package com.teparak.graderbot.utils


import org.apache.commons.lang3.RandomUtils
import org.apache.commons.text.CharacterPredicate
import org.apache.commons.text.CharacterPredicates
import org.apache.commons.text.RandomStringGenerator
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component
import java.util.*
import javax.annotation.PostConstruct

@Component
@Configuration
@ConfigurationProperties("random.uuid")
class RandomFactory {

    var maxLength: Int = 10
    var minLength: Int = 5
    var maxRetry: Int = 5

    @PostConstruct
    private fun validation() {

        if (minLength < 1) throw IllegalArgumentException("minimum length must be > 0")
        if (maxLength < 1) throw IllegalArgumentException("maximum length must be > 0")
        if (maxRetry < 0) throw IllegalArgumentException("maximum random retry must be 0 or greater")

        if (minLength > maxLength) throw IllegalArgumentException("minimum length cannot be greater than maximum length | min len: $minLength max len: $maxLength")
        if (maxLength < minLength) throw IllegalArgumentException("maximum length cannot be greater than minimum length | min len: $minLength max len: $maxLength")
    }

    fun alphanumerical(length: Int): String {
        return random(true, true, true, length)
    }

    fun alphanumerical(minLengthInclusive: Int = minLength, maxLengthInclusive: Int = maxLength): String {
        return random(true, true, true, RandomUtils.nextInt(minLengthInclusive, maxLengthInclusive))
    }

    fun alphanumericalIgnoreCase(length: Int): String {
        return random(true, false, true, length)
    }

    fun alphanumericalIgnoreCase(minLengthInclusive: Int = minLength, maxLengthInclusive: Int = maxLength): String {
        return random(true, false, true, RandomUtils.nextInt(minLengthInclusive, maxLengthInclusive))
    }

    fun numericalOnly(): String {
        return random(false, false, true, RandomUtils.nextInt(minLength, maxLength))
    }

    fun numericalOnly(length: Int): String {
        return random(false, false, true, length)
    }

    fun numericalOnly(minLengthInclusive: Int, maxLengthInclusive: Int): String {
        return random(false, false, true, RandomUtils.nextInt(minLengthInclusive, maxLengthInclusive))
    }

    fun lowerAlphabetOnly(): String {
        return random(true, false, false, RandomUtils.nextInt(minLength, maxLength))
    }

    fun lowerAlphabetOnly(length: Int): String {
        return random(true, false, false, length)
    }

    fun lowerAlphabetOnly(minLengthInclusive: Int, maxLengthInclusive: Int): String {
        return random(true, false, false, RandomUtils.nextInt(minLengthInclusive, maxLengthInclusive))
    }

    fun upperAlphabetOnly(): String {
        return random(false, true, false, RandomUtils.nextInt(minLength, maxLength))
    }

    fun upperAlphabetOnly(length: Int): String {
        return random(false, true, false, length)
    }

    fun upperAlphabetOnly(minLengthInclusive: Int, maxLengthInclusive: Int): String {
        return random(false, true, false, RandomUtils.nextInt(minLengthInclusive, maxLengthInclusive))
    }

    private fun random(lowercaseLetters: Boolean, uppercaseLetters: Boolean, numbers: Boolean, length: Int): String {
        val numerical = CharacterPredicates.ARABIC_NUMERALS
        val upperLetters = CharacterPredicates.ASCII_UPPERCASE_LETTERS
        val lowerLetters = CharacterPredicates.ASCII_LOWERCASE_LETTERS

        val store = LinkedList<CharacterPredicate>()

        if (!lowercaseLetters && !uppercaseLetters && !numbers) throw IllegalArgumentException()

        if (lowercaseLetters) store.add(lowerLetters)
        if (uppercaseLetters) store.add(upperLetters)
        if (numbers) store.add(numerical)

        val randomGenerator = RandomStringGenerator.Builder()
                .filteredBy(*store.toTypedArray())
                .build()
        return randomGenerator.generate(length)


    }
}