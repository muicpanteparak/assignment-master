package com.teparak.graderbot.utils.entity

import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.LastModifiedBy
import javax.persistence.MappedSuperclass

@MappedSuperclass
abstract class AbstractAuditorEntity : AbstractEntity() {

    @CreatedBy
    var createdBy: String? = null

    @LastModifiedBy
    var modifiedBy: String? = null
}