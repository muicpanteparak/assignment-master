package com.teparak.graderbot.assignment.master

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class AssignmentMasterApplicationTests {

    @Test
    fun contextLoads() {
    }

}
